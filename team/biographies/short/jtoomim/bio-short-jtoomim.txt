Jonathan Toomim is an independent BCH developer, and an active contributor to the BCHN project.

He has been involved with the big block Bitcoin movement since 2015.
He contributed as a junior developer to Bitcoin XT.
In December 2015, he presented data on [block propagation performance on a BIP101 testnet](https://youtu.be/EfuRTDcsWrk?t=8161).
After that, he surveyed miner opinions on preferred blocksizes in the [Blocksize Consensus Census](https://www.reddit.com/r/Bitcoin/comments/3ygu5d/blocksize_consensus_census/), and helped launch https://bitcoin.consider.it/ to richly represent users', developers', miners', and businesses' opinions on scaling proposals.
In 2016, He cofounded Bitcoin Classic, an almost-successful attempt to increase the
Bitcoin block size limit to 2 MB with a consensus hard fork.
In 2018, he [analyzed stress test data](https://medium.com/@j_73307/block-propagation-data-from-bitcoin-cashs-stress-test-5b1d7d39a234), and came up with the [Xthinner block propagation protocol](https://medium.com/@j_73307/benefits-of-ltor-in-block-entropy-encoding-or-8d5b77cc2ab0).
In 2019, he implemented a non-production-ready version of Xthinner and [ran it on mainnet](https://www.reddit.com/r/btc/comments/bgr143/xthinner_mainnet_compression_performance_stats/).
He also built a benchmark system that showed that up to [3,000 tx/sec](https://www.youtube.com/watch?v=j5UvgfWVnYg) is possible with simple
transactions on commodity hardware with a slightly modified version of Bitcoin ABC as long as
networking isn't the bottleneck.

His work on BCHN is mostly focused on scalability issues, like Xthinner and the O(n^2) transaction chain issue.

More information on Jonathan's work on BCH can be found in his Flipstarter campaign at https://flipstarter.toom.im/.