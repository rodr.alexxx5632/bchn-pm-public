fpelliccioni
Fernando Pelliccioni
BCHN contributor, Knuth node lead developer.

Fernando comes from a programmer's house, his father is a programmer, 
which allowed him to learn the discipline from childhood.

In 1997 he started working professionally and began to specialize in 
systems programming and high performance computing.
While he can program in multiple programming languages ​​and in different 
paradigms, his passion for efficiency made him choose C++ as his main 
programming language. He has been programming in this language for more 
than 23 years and his interest in it remains intact.
Although he is not a voting member of the C++ standards committee, 
he has constant dialogue with its members and actively participates in 
the evolution of the C++ programming language.

In addition to systems programming, he is driven by mathematics, algorithms 
and formal software specification. He has dialogue with eminences in these 
areas, such as Paul McJones, Bertrand Meyer, Alexander Stepanov and 
Donald Knuth himself.

In 2016, he was awarded with the highest distinction granted by the 
Argentine Parliament, receiving the Honorable Mention 
"Senator Domingo Faustino Sarmiento", for his work as CS leader of the 
COPLA team (National Commission of the Outer Limit of the Continental Shelf), 
which enabled the determination of the new Outer Limit of the Continental Shelf
of the Argentine Republic.
The limit was approved on March 11, 2016 by the United Nations, unanimously. 
The deep technical and scientific work carried out made possible the extension 
of the Argentine territory in more than 1,782,000 km2, becoming the most 
important event, at the national level, since the country's independence 
processes.

Also, in 2016 he began to get involved in the world of cryptocurrencies, 
creating the first versions of the Bitprim node, then becoming the team's 
lead programmer.
He currently serves as the lead developer for Knuth project, a high-performance
multi-currency node, and is a member and contributor of BCHN.
His purpose is to try to bring all his knowledge about algorithms, 
high-performance computing and modern C++ development to the Bitcoin world.
